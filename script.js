const roles = {
    admin: 'https://www.svgrepo.com/show/396213/judge-light-skin-tone.svg',
    student: 'https://www.svgrepo.com/show/398408/student-medium-light-skin-tone.svg',
    lector: 'https://www.svgrepo.com/show/398447/teacher.svg',
};

const gradation = {
    20: 'satisfactory',
    55: 'good',
    85: 'very-good',
    100: 'excellent',
};

const users = [
    {
        name: 'Jack Smith',
        age: 23,
        img: 'https://www.svgrepo.com/show/228291/user-profile.svg',
        role: 'student',
        courses: [
            {
                title: 'Front-end Pro',
                mark: 20,
            },
            {
                title: 'Java Enterprise',
                mark: 100,
            },
        ],
    },
    {
        name: 'Amal Smith',
        age: 20,
        img: 'https://www.svgrepo.com/show/228291/user-profile.svg',
        role: 'student',
    },
    {
        name: 'Noah Smith',
        age: 43,
        img: 'https://www.svgrepo.com/show/228291/user-profile.svg',
        role: 'student',
        courses: [
            {
                title: 'Front-end Pro',
                mark: 50,
            },
        ],
    },
    {
        name: 'Charlie Smith',
        age: 18,
        img: 'https://www.svgrepo.com/show/228291/user-profile.svg',
        role: 'student',
        courses: [
            {
                title: 'Front-end Pro',
                mark: 75,
            },
            {
                title: 'Java Enterprise',
                mark: 23,
            },
        ],
    },
    {
        name: 'Emily Smith',
        age: 30,
        img: 'https://www.svgrepo.com/show/228291/user-profile.svg',
        role: 'admin',
        courses: [
            {
                title: 'Front-end Pro',
                score: 10,
                lector: 'Leo Smith',
            },
            {
                title: 'Java Enterprise',
                score: 50,
                lector: 'David Smith',
            },
            {
                title: 'QA',
                score: 75,
                lector: 'Emilie Smith',
            },
        ],
    },
    {
        name: 'Leo Smith',
        age: 53,
        img: 'https://www.svgrepo.com/show/228291/user-profile.svg',
        role: 'lector',
        courses: [
            {
                title: 'Front-end Pro',
                score: 78,
                studentsScore: 79,
            },
            {
                title: 'Java Enterprise',
                score: 85,
                studentsScore: 85,
            },
            {
                title: 'Java Enterprise',
                score: 30,
                studentsScore: 20,
            },
        ],
    },
];

const markGradation = (obj, mark) => {
    let grad;
    for (let key in obj) {
        if (mark <= key) {
            grad = obj[key];
            break;
        }
    }
    return grad;
};

const capitalizeFirstLetter = (grad) => {
    return grad[0].toUpperCase() + grad.slice(1);
};

class User {
    constructor(name, age, img, role, courses) {
        this.name = name;
        this.age = age;
        this.img = img;
        this.role = role;
        this.courses = courses;
    }

    render() {
        return `
            <div class="user">
                <div class="user__info">
                    <div class="user__info--data">
                        <img src="${this.img}" alt="${this.name}" height="50" />
                        <div class="user__naming">
                            <p>Name: <b>${this.name}</b></p>
                            <p>Age: <b>${this.age}</b></p>
                        </div>
                    </div>
                    <div class="user__info--role ${this.role}">
                        <img src="${roles[this.role]}" alt="${this.role}" height="25"/>
                        <p>${this.role}</p>
                    </div>
                </div>
                ${this.courses ? this.renderCourses() : ''}
</div>
		`;
    }

    renderCourses() {
        let allTitle = this.courses
            .map((el) => {
                return `
                    <div class="user__courses--course ${this.role}">
                        <p>Title: <b>${el.title}</b>
                            <span class="${markGradation(gradation, el.mark)}">
                                ${capitalizeFirstLetter(markGradation(gradation, el.mark))}
                            </span>
                        </p>
                     </div>`;
            })
            .join('');

        return `<div class="user__courses admin--info">${allTitle}</div>`;
    }
}

class Student extends User {
    constructor(name, age, img, role, courses) {
        super(name, age, img, role, courses);
    }
}

class Admin extends User {
    constructor(name, age, img, role, courses) {
        super(name, age, img, role, courses);
    }

    renderCourses() {
        let allTitle = this.courses
            .map((el) => {
                return `
                    <div class="user__courses--course ${this.role}"> 
                        <p>Title: <b>${el.title}</b></p> 
                        <p>Admin's score:
                            <span class="${markGradation(gradation, el.score)}">
                                ${capitalizeFirstLetter(markGradation(gradation, el.score))}
                            </span>
                        </p> 
                        <p>Lector: <b>${el.lector}</b></p> 
                    </div>`;
            })
            .join('');

        return `<div class="user__courses admin--info">${allTitle}</div>`;
    }
}
class Lector extends User {
    constructor(name, age, img, role, courses) {
        super(name, age, img, role, courses);
    }

    renderCourses() {
        let allTitle = this.courses
            .map((el) => {
                return `
                    <div class="user__courses--course ${this.role}"> 
                        <p>Title: <b>${el.title}</b></p> 
                        <p>Lector's score:
                            <span class="${markGradation(gradation, el.score)}">
                                ${capitalizeFirstLetter(markGradation(gradation, el.score))}
                            </span>
                        </p> 
                        <p>Average student's score:
                            <b class="${markGradation(gradation, el.studentsScore)}">
                                ${capitalizeFirstLetter(markGradation(gradation, el.studentsScore))}
                            </b>
                        </p> 
                    </div>`;
            })
            .join('');

        return `<div class="user__courses admin--info">${allTitle}</div>`;
    }
}

const ROLES = {
    student: (...user) => new Student(...user),
    admin: (...user) => new Admin(...user),
    lector: (...user) => new Lector(...user),
};

const newRole = users.map((user) => ROLES[user.role](user.name, user.age, user.img, user.role, user.courses));

const arrRoles = newRole.map((user) => {
    return user.render();
});

document.write(`
	<div class="users">
		${arrRoles.join('')}
  	</div>
`);
